1.Mengambil data dari database:

A.Mengambil data user kecuali password=

SELECT id, name, email FROM user;

B.Mengambil data table item 

-Mengambil data dengan harga diatas 1000000=

SELECT * FROM items WHERE price > 1000000;

-Mengambil data yang memiliki kata sang=

SELECT * FROM items WHERE description LIKE "%sang%";

C. Menampilkan data items join dengan kategori=

SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori FROM items INNER JOIN categories on items.category_id = categories.id;

2.Mengubah data dalam table:

UPDATE items set price=2500000 WHERE id=1;